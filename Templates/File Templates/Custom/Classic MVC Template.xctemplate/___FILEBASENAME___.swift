//
// ___FILENAME___
// ___PROJECTNAME___
//
// Created by ___FULLUSERNAME___ on ___DATE___.
// ___COPYRIGHT___
//

import UIKit

/// Краткое описание класса
class ___FILEBASENAMEASIDENTIFIER___: UIViewController {
    
    // MARK: Переменные
    // @IBOutlet weak var
    
    // public var
    
    // private var
    
    // etc
    
    
    // MARK: Life cycle (Жизненный цикл класса)
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    // Вот-вот отобразится
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupView()
        setupData()
        
    }
    
    // Уже отобразился
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    // Вот-вот уйдем с экрана
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    // Ушли с экрана
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
    }
    
    // Сообщение об деинициализации контроллера (required)
    deinit {
        #if DEBUG
            print("✅ \(type(of: self)): deinit")
        #endif
    }
    
    // MARK: Main logic (Основная логика класса)
    
    /// Настройка визуальной части элементов
    private func setupView() {
        
    }
    
    /// Заполнение данных на экране
    private func setupData() {
        
    }
    
}
